var chai = require('chai');
var chaiHttp = require('chai-http');
const assert = require('assert').strict;
chai.use(chaiHttp);

describe('Juego', function () {
    it('Se logran simular partidas', function () {
        // Respuesta esperada de la API
        var expect = "Partida Simulada";

        // Realizamos la simulacion de la conexion y peticion
        chai.request('http://localhost:4000').keepOpen()
        .post('/simular/')
        .type('form')
        .set('content-type', 'application/json')
        .send(JSON.stringify({
            id: '2e19458c-1814-11eb-adc1-0242ac120002',
            jugadores: [
                0,
                1
            ]
        }))
        .end(function (err, res) {
            //¿Respuesta esperada?
            assert.notEqual(res.body.data, expect);
        });
    });
});


describe('Torneo', function () {
    it('Se obtiene respuesta del torneo', function () {
        // Respuesta esperada de la API
        var expect = "Api torneo lista";

        // Realizamos la simulacion de la conexion y peticion
        chai.request('http://localhost:8000').keepOpen()
        .get('/')
        .end(function (err, res) {
            //¿Respuesta esperada?
            var json = res.text;
            assert.equal(json, expect);
        });
    });
});


describe('Dado', function () {
    it('Se obtiene el lanzamiento de dados', function () {
        // Respuesta esperada de la API
        var expect = 5;

        // Realizamos la simulacion de la conexion y peticion
        chai.request('http://localhost:5000').keepOpen()
        .get('/tirar/'+5)
        .end(function (err, res) {
            //¿Respuesta esperada?
            
            var json = JSON.parse(JSON.stringify(res.body));
            console.log(json.dados.lenght);
            //assert.equal(json.dados.lenght, expect);
        });
    });
});


describe('JWT', function () {
    it('Se obtiene respuesta del Servidores de Token', function () {
        // Respuesta esperada de la API
        var expect = "SERVIDOR JWT ENCENDIDO";

        // Realizamos la simulacion de la conexion y peticion
        chai.request('http://localhost:10000').keepOpen()
        .get('/')
        .end(function (err, res) {
            //¿Respuesta esperada?
            var json = res.text;
            assert.equal(json, expect);
        });
    });
});