## Test

Para realizar las pruebas se utilizo assert, chai y mocha, por lo cual se instalo:
```
>>> npm install chai chai-http mocha assert --save-dev
```

Para ejecutar las pruebas se utiliza el siguiente comando:
```
>>> npm run test
```