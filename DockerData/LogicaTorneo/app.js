"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var express = require('express');
var cors = require('cors');
var bodyParser = require('body-parser');
var fs = require('fs');
var axios = require('axios');
var mysql = require('mysql');
var path = require('path');
var crypto = require("crypto");
var auth = require(path.resolve(__dirname, './authToken.js'));
var app = express();
app.use(bodyParser.json());
app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
/*Variables de entorno donde correra la API Torneo*/
var ip = process.env.IP || "torneo";
var port = process.env.PORT || 8000;
/*Variables de entorno donde correra la API Usuario*/
var usersip = process.env.USERIP || "user";
var usersport = process.env.USERPORT || 9000;
/*Variables de entorno para la conexion con la BD*/
var DBHOST = process.env.DBHOST || "torneodb";
var USER = process.env.DBUSERNAME || "root";
var PASS = process.env.DBPASSWORD || "123456789";
var DBNAME = process.env.DBNAME || "torneoDB";
//========================================= BD MYSQL =========================================//
/*Conexion con la BD MYSQL*/
var conn = mysql.createConnection({
    host: DBHOST,
    user: USER,
    password: PASS,
    database: DBNAME
});
//========================================== TORNEO ==========================================//
app.get("/", function (req, res) {
    res.send("Api torneo lista");
});
app.get("/listaTorneo/", function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var sql;
    return __generator(this, function (_a) {
        sql = "SELECT * FROM torneo;";
        conn.query(sql, function (err, result) {
            if (err)
                res.status(406).send(JSON.stringify([err]));
            res.status(201).send(JSON.stringify(result));
        });
        return [2 /*return*/];
    });
}); });
app.post("/selectTorneo/", function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var id, sql;
    return __generator(this, function (_a) {
        id = req.body.id;
        sql = "SELECT * FROM llave,partido,torneo WHERE partido.id_partido=llave.id_partido AND partido.id_torneo=torneo.id_torneo AND torneo.id_torneo=" + id + ";";
        conn.query(sql, function (err, result) {
            if (err)
                res.status(406).send(JSON.stringify([err]));
            res.status(201).send(JSON.stringify(result));
        });
        return [2 /*return*/];
    });
}); });
app.post("/crearTorneo/", function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var idJuego, urlJuego, jugadores, cantJug, par, llave_valida, i, partidas, idTorneo, jornadas, cont1, cont2, idpartidas, index, p1, p2, newuuid, idPartido, index, newuuid, idPartido, i, pos, j;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                idJuego = req.body.jid;
                urlJuego = req.body.url;
                jugadores = req.body.jugadores;
                cantJug = jugadores.length;
                par = jugadores.length / 2;
                llave_valida = false;
                for (i = 0; i <= 10; i++) {
                    //validamos si es llave de 0 a 10
                    if (Math.pow(2, i) == par) {
                        llave_valida = true;
                        break;
                    }
                }
                if (!(Number(cantJug) % 2 == 0 && llave_valida)) return [3 /*break*/, 12];
                partidas = Number(cantJug) / 2;
                return [4 /*yield*/, crearTorneo(idJuego, urlJuego, partidas)];
            case 1:
                idTorneo = _a.sent();
                jornadas = new Array();
                cont1 = -2;
                cont2 = -1;
                jugadores.sort(function (a, b) { return (Math.random() - 0.5); });
                idpartidas = new Array();
                index = 0;
                _a.label = 2;
            case 2:
                if (!(index < partidas)) return [3 /*break*/, 5];
                cont1 = cont1 + 2;
                cont2 = cont2 + 2;
                p1 = jugadores[cont1];
                p2 = jugadores[cont2];
                newuuid = crypto.randomBytes(16).toString("hex");
                return [4 /*yield*/, crearPartidas(idTorneo, newuuid, p1, p2)];
            case 3:
                idPartido = _a.sent();
                idpartidas.push(newuuid);
                _a.label = 4;
            case 4:
                index++;
                return [3 /*break*/, 2];
            case 5:
                jornadas.push(idpartidas);
                //========================= PROXIMAS JORNADAS =========================//
                partidas = partidas / 2;
                _a.label = 6;
            case 6:
                if (!(partidas >= 1)) return [3 /*break*/, 11];
                //creamos las siguientes jornadas
                idpartidas = new Array();
                index = 0;
                _a.label = 7;
            case 7:
                if (!(index < partidas)) return [3 /*break*/, 10];
                newuuid = crypto.randomBytes(16).toString("hex");
                return [4 /*yield*/, crearPartidas(idTorneo, newuuid, 0, 0)];
            case 8:
                idPartido = _a.sent();
                idpartidas.push(newuuid);
                _a.label = 9;
            case 9:
                index++;
                return [3 /*break*/, 7];
            case 10:
                jornadas.push(idpartidas);
                partidas = partidas / 2;
                return [3 /*break*/, 6];
            case 11:
                //========================= FLUJO DEL TORNEO =========================//
                for (i = 1; i < jornadas.length; i++) {
                    pos = 0;
                    for (j = 0; j < jornadas[i].length; j++) {
                        crearflujo(jornadas[i][j], jornadas[i - 1][pos], jornadas[i - 1][pos + 1]);
                        pos += 2;
                    }
                }
                fs.appendFile('./Log/bitacora.txt', "Torneo Creado:" + idTorneo + " Partidas:" + jornadas.length + ".\n", function () { });
                res.send(JSON.stringify({ 201: "Torneo Creado", "TorneoId": idTorneo, "Partidas:": jornadas }));
                return [3 /*break*/, 13];
            case 12:
                res.send(JSON.stringify({ 406: "Número de jugadores inválido" }));
                _a.label = 13;
            case 13: return [2 /*return*/];
        }
    });
}); });
function crearTorneo(idJuego, urlJuego, partidas) {
    return new Promise(function (resolve) {
        var sql = "INSERT INTO torneo (id_juego, url_juego, n_partidas) VALUES (" + idJuego + ",'" + urlJuego + "'," + partidas + ");";
        conn.query(sql, function (err, res) {
            if (err)
                return;
            resolve(res.insertId);
        });
    });
}
function crearPartidas(idTorneo, idPartida, idJ1, idJ2) {
    var sql = "INSERT INTO partido (id_partido,id_torneo) VALUES ('" + idPartida + "'," + idTorneo + ");";
    conn.query(sql, function (err, result) {
        if (err)
            return;
        var sqlp = "INSERT INTO llave (id_partido,id_jugador1,id_jugador2) VALUES ('" + idPartida + "'," + idJ1 + "," + idJ2 + ");";
        conn.query(sqlp, function (err, result) {
            if (err)
                return;
            return;
        });
    });
}
function crearflujo(idAct, idPre1, idPre2) {
    var sql = "INSERT INTO flujoPartida (idAct,idPre1,idPre2) VALUES ('" + idAct + "','" + idPre1 + "','" + idPre2 + "');";
    conn.query(sql, function (err, result) {
        if (err)
            return;
        return;
    });
}
function setMarcador(idPartida, mp1, mp2) {
    var sql = "UPDATE llave SET score_jugador1=" + mp1 + ", score_jugador2=" + mp2 + " WHERE id_partido='" + idPartida + "';";
    conn.query(sql, function (err, result) {
        if (err)
            console.log(err);
        console.log("Marcador de partida actualizado: " + idPartida);
    });
}
function ganador(idPartida, ganador) {
    //ganador = [1, 2]
    var sql = "SELECT id_jugador" + ganador + " AS jugador FROM llave WHERE id_partido='" + idPartida + "';";
    conn.query(sql, function (err, result) {
        if (err)
            console.log(err);
        if (result.length > 0) {
            var winner = result[0].jugador; //id del ganador
            partidaSiguiente(idPartida, winner);
        }
        else {
            console.log("No se encontro la partida");
        }
    });
}
function partidaSiguiente(idPartida, jugador) {
    //vemos si toca local
    var sqll = "SELECT idAct FROM flujoPartida WHERE idPre1='" + idPartida + "';";
    conn.query(sqll, function (err, result) {
        if (err)
            console.log(err);
        if (result.length > 0) {
            //actualizamos esa partida
            actPartida(result[0].idAct, jugador, true);
        }
        else {
            //vemos si es visita
            var sqlv = "SELECT idAct FROM flujoPartida WHERE idPre2='" + idPartida + "';";
            conn.query(sqlv, function (err, result) {
                if (err)
                    console.log(err);
                if (result.length > 0) {
                    //actualizamos esa partida
                    actPartida(result[0].idAct, jugador, false);
                }
            });
        }
    });
}
function actPartida(idPartida, idJugador, tipo) {
    // tipo=true local, tipo=false visita
    var p = "id_jugador2"; //visita
    if (tipo) {
        p = "id_jugador1"; //local
    }
    var sql = "UPDATE llave SET " + p + "=" + idJugador + " WHERE id_partido='" + idPartida + "';";
    conn.query(sql, function (err, result) {
        if (err)
            console.log(err);
        fs.appendFile('./Log/bitacora.txt', "Actualizacion en flujo de partidas.\n", function () { });
        console.log("Se actualizo el flujo de las partidas");
    });
}
app.put('/partidas/:id', function (req, res) {
    var partida = req.params.id; // id de la partida
    var m = req.body.marcador; // marcador
    if (m[0] > m[1]) {
        fs.appendFile('./Log/bitacora.txt', "Partida:" + partida + " Ganador: Local.\n", function () { });
        console.log("Ganador Jugador 1");
        //setear el marcador
        setMarcador(partida, m[0], m[1]);
        ganador(partida, "1");
    }
    else if (m[1] > m[0]) {
        fs.appendFile('./Log/bitacora.txt', "Partida:" + partida + " Ganador: Visita.\n", function () { });
        console.log("Ganador Jugador 2");
        //setear el marcador
        setMarcador(partida, m[0], m[1]);
        ganador(partida, "2");
    }
    res.send(JSON.stringify({ 201: "Ganador asignado", "partida": partida }));
});
//====================================== JUEGO ======================================//
app.get("/listaJuego", function (req, res) {
    var sql = "SELECT * FROM juego;";
    conn.query(sql, function (err, result) {
        if (err)
            res.status(406).send(JSON.stringify([err]));
        res.status(201).send(JSON.stringify(result));
    });
});
app.post("/insertJuego", function (req, res) {
    var sql = "INSERT INTO juego(link,nombre) VALUES('" + req.body.link + "','" + req.body.nombre + "');";
    conn.query(sql, function (err, result) {
        if (err)
            res.status(406).send(JSON.stringify({ 406: "Error en realizar insert de juegos" }));
        res.status(201).send(JSON.stringify({ 201: "Juego insertado" }));
    });
});
app.post("/updateJuego", function (req, res) {
    var sql = "UPDATE juego SET link='" + req.body.link + "', nombre='" + req.body.nombre + "' WHERE id_juego=" + req.body.id + ";";
    conn.query(sql, function (err, result) {
        if (err)
            res.status(406).send(JSON.stringify({ 406: "Error en realizar update de juegos" }));
        res.send(JSON.stringify({ 201: "Juego actualizado" }));
    });
});
app.post("/deleteJuego", function (req, res) {
    var sql = "DELETE FROM juego WHERE id_juego=" + req.body.id + ";";
    conn.query(sql, function (err, result) {
        if (err)
            res.status(406).send(JSON.stringify({ 406: "Error en realizar delete de juegos" }));
        res.status(201).send(JSON.stringify({ 201: "Juego eliminado" }));
    });
});
//===================================== USUARIO =====================================//
app.get("/listaUsuario/", auth.authtoken, function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    return __generator(this, function (_a) {
        axios.get('http://' + usersip + ':' + usersport + '/jugadores/jugadores/')
            .then(function (response) {
            res.send(JSON.stringify(response.data));
        }).catch(function (error) {
            res.send(error.message);
        });
        return [2 /*return*/];
    });
}); });
app.post("/insertUsuario/", auth.authtoken, function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    return __generator(this, function (_a) {
        axios.post('http://' + usersip + ':' + usersport + '/jugadores/', {
            email: req.body.email,
            password: req.body.password,
            nombres: req.body.nombre,
            apellidos: req.body.apellido,
            administrador: req.body.admin
        }).then(function (response) {
            res.send(JSON.stringify(response.data));
        }).catch(function (error) {
            res.send(error.message);
        });
        return [2 /*return*/];
    });
}); });
app.post("/updateUsuario/", auth.authtoken, function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    return __generator(this, function (_a) {
        axios.put('http://' + usersip + ':' + usersport + '/jugadores/' + req.body.id, {
            email: req.body.email,
            password: req.body.password,
            nombres: req.body.nombre,
            apellidos: req.body.apellido,
            administrador: req.body.admin
        }).then(function (response) {
            res.send(JSON.stringify(response.data));
        }).catch(function (error) {
            res.send(error.message);
        });
        return [2 /*return*/];
    });
}); });
app.post("/deleteUsuario/", auth.authtoken, function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    return __generator(this, function (_a) {
        //No hay un endpoint definido en la api para usuarios
        res.status(201).send("No se puede eliminar usuarios");
        return [2 /*return*/];
    });
}); });
app.post("/login/", auth.authtoken, function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var email, pass;
    return __generator(this, function (_a) {
        email = req.body.email;
        pass = req.body.password;
        axios.get('http://' + usersip + ':' + usersport + '/login/?email=' + email + '&password=' + pass)
            .then(function (response) {
            res.send(JSON.stringify(response.data));
        }).catch(function (error) {
            res.send(error.message);
        });
        return [2 /*return*/];
    });
}); });
//=================================================================================//
app.listen(port, ip, function () { return __awaiter(void 0, void 0, void 0, function () {
    return __generator(this, function (_a) {
        console.log('Torneo se escucha en el puerto: %d y con la ip: %s', port, ip);
        return [2 /*return*/];
    });
}); });
