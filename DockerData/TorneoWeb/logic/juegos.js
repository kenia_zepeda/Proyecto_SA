
function initGame(){
    var logeado = sessionStorage.getItem('logueado');
    if(!logeado){
        window.location.href = "../";
    }

    //mostrar el nombre del usuario
    var nombre = sessionStorage.getItem('nombre-usuario');
    document.getElementById("nombre_user").innerText = nombre;

    //limpiamos los campos
    limpiar();

    //cargamos los dropdown
    fetch('../listaJuego',{
        method: 'GET'
    }).then(res => res.json())
    .catch(error => console.error('Error:', error))
    .then(response => viewGames(response));
}

function regresar(){
    window.location.href = "../index";
}


//==================================== SELECT ====================================//

function viewGames(response){
    var array = JSON.parse(JSON.stringify(response));

    //lista de juegos para modificar
    var upd = document.getElementById("juego_m");

    //lista de juegos para eliminar
    var del = document.getElementById("juego_e");

    array.forEach(element => {
        var btnu = document.createElement("button");
        btnu.setAttribute('id',element.id_juego);
        btnu.setAttribute('name',element.link);
        btnu.setAttribute('onclick','selectGame(this);');
        btnu.setAttribute("class","dropdown-item");
        btnu.innerText = element.nombre;

        upd.appendChild(btnu);

        var btnd = document.createElement("button");
        btnd.setAttribute('id',element.id_juego);
        btnd.setAttribute('name',element.link);
        btnd.setAttribute('onclick','deleteGame(this);');
        btnd.setAttribute("class","dropdown-item");
        btnd.innerText = element.nombre;

        del.appendChild(btnd);
    });
}


//===================================== CREAR =====================================//

function addGame(){
    var nombre = document.getElementById("a-nombre").value;
    var link = document.getElementById("a-link").value;

    if(nombre != "" && link != ""){
        fetch('../insertJuego',{
            method: 'POST',
            body: JSON.stringify({"Jid":0,"Name":nombre,"URL":link}),
            headers:{
              'Content-Type': 'application/json'
            }
        }).then(res => res.json())
        .catch(error => console.error('Error:', error))
        .then(response => location.reload());
    }
}


//=================================== MODIFICAR ===================================//

function selectGame(btn){
    //id del juego
    document.getElementById("m-id").textContent = btn.id;
    //nombre del juego
    document.getElementById("m-nombre").value = btn.innerText;
    //link del juego
    document.getElementById("m-link").value = btn.name;
}

function updateGame(){
    var id = document.getElementById("m-id").textContent;
    var nombre = document.getElementById("m-nombre").value;
    var link = document.getElementById("m-link").value;

    fetch('../updateJuego',{
        method: 'POST',
        body: JSON.stringify({"Jid":id,"Name":nombre,"URL":link}),
        headers:{
          'Content-Type': 'application/json'
        }
    }).then(res => res.json())
    .catch(error => console.error('Error:', error))
    .then(response => location.reload());
}


//==================================== ELIMINAR ====================================//

function deleteGame(btn){
    var id = btn.id;

    fetch('../deleteJuego',{
        method: 'POST',
        body: JSON.stringify({"Jid":id,"Name":"","URL":""}),
        headers:{
          'Content-Type': 'application/json'
        }
    }).then(res => res.json())
    .catch(error => console.error('Error:', error))
    .then(response => location.reload());
}


//==================================== LIMPIAR ====================================//

function limpiar(){
    document.getElementById("a-nombre").value = '';
    document.getElementById("a-link").value = '';

    document.getElementById("m-id").style.visibility = "hidden";
    document.getElementById("m-id").textContent = '';
    document.getElementById("m-nombre").value = '';
    document.getElementById("m-link").value = '';
}