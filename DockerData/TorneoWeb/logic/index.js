
function initTorneo(){
    var logeado = sessionStorage.getItem('logueado');
    if(!logeado){
        window.location.href = "../";
    }

    //mostrar el nombre del usuario
    var nombre = sessionStorage.getItem('nombre-usuario');
    document.getElementById("nombre_user").innerText = nombre;


    //llenamos el dropdown de juegos
    fetch('../listaJuego',{
        method: 'GET'
    }).then(res => res.json())
    .catch(error => console.error('Error:', error))
    .then(response => viewGames(response));

    //llenamos el dropdown de jugadores
    fetch('../listaUsuario',{
        method: 'GET'
    }).then(res => res.json())
    .catch(error => console.error('Error:', error))
    .then(response => viewUser(response));

    //llenamos el dropdown de torneos
    fetch('../listaTorneo',{
        method: 'GET'
    }).then(res => res.json())
    .catch(error => console.error('Error:', error))
    .then(response => viewTorneo(response));
}

function usuarios(){
    window.location.href = "../usuarios";
}

function juegos(){
    window.location.href = "../juegos";
}

function logout(){
    sessionStorage.clear();
    window.location.href = "../";
}


//==================================== LISTA USUARIOS ====================================//

function viewUser(response){
    var array = JSON.parse(JSON.stringify(response));
    var jugadores = document.getElementById("jugador-lista");

    array.forEach(element => {
        var btn = document.createElement("button");
        btn.setAttribute('id',element.id);
        btn.setAttribute('onclick','addUser(this);');
        btn.setAttribute("class","dropdown-item");
        btn.innerText = element.nombres;

        jugadores.appendChild(btn);
    });
}

var array_user = [];

function addUser(btn){
    var iduser = btn.id;
    btn.disabled = true;
    array_user.push(iduser);

    var num = document.getElementById("num-players");
    num.innerText = "Participantes: " + array_user.length;
}


//==================================== LISTA JUEGOS ====================================//

function viewGames(response){
    var array = JSON.parse(JSON.stringify(response));
    var dropdown = document.getElementById("juegos-lista");

    array.forEach(element => {
        var btn = document.createElement("button");
        btn.setAttribute('id',element.id_juego);
        btn.setAttribute('name',element.link);
        btn.setAttribute('onclick','addGame(this);');
        btn.setAttribute("class","dropdown-item");
        btn.innerText = element.nombre;

        dropdown.appendChild(btn);
    });
}

var urlgame = "";
var idgame = "";

function addGame(btn){
    urlgame = btn.name;
    idgame = btn.id;

    var select = document.getElementById("game-select");
    select.innerText = "Juego Elegido: " + btn.innerText;
}


//==================================== LISTA TORNEOS ====================================//

function viewTorneo(response){
    var array = JSON.parse(JSON.stringify(response));
    var torneos = document.getElementById("torneos-lista");

    array.forEach(element => {
        var btn = document.createElement("button");
        btn.setAttribute('id',element.id_torneo);
        btn.setAttribute('name',element.url_juego);
        btn.setAttribute('onclick','selectTorneo(this);');
        btn.setAttribute("class","dropdown-item");
        btn.innerText = "Torneo " + element.id_torneo;

        torneos.appendChild(btn);
    });
}


function selectTorneo(btn){
    var id = btn.id; //id del torneo

    fetch('../selectTorneo', {
        method: 'POST',
        body: JSON.stringify({"Tid":id}),
        headers:{
          'Content-Type': 'application/json'
        }
    }).then(res => res.json())
    .catch(error => console.error('Error:', error))
    .then(response => setPartidos(response));
}


function setPartidos(response){
    var array = JSON.parse(JSON.stringify(response));
    var tbody = document.getElementById("tbody");
    tbody.innerHTML = '';

    array.forEach(element => {
        var tr = document.createElement("tr");

        var tdp = document.createElement("td");
        tdp.innerText = element.id_partido;

        var tdj1 = document.createElement("td");
        tdj1.innerText = element.id_jugador1;

        var tdj2 = document.createElement("td");
        tdj2.innerText = element.id_jugador2;

        var tds1 = document.createElement("td");
        tds1.innerText = element.score_jugador1;

        var tds2 = document.createElement("td");
        tds2.innerText = element.score_jugador2;

        var tdb = document.createElement("td");
        var name = element.url_juego+","+element.id_jugador1+","+element.id_jugador2;

        var btng = document.createElement("button");
        btng.setAttribute('id',element.id_partido);
        btng.setAttribute('name',name);
        btng.setAttribute('onclick','generar(this);');
        btng.setAttribute("class","d-inline-block btn btn-info");
        btng.innerText = "Generar";

        var btns = document.createElement("button");
        btns.setAttribute('id',element.id_partido);
        btns.setAttribute('name',name);
        btns.setAttribute('onclick','simular(this);');
        btns.setAttribute("class","d-inline-block btn btn-success");
        btns.innerText = "Simular";

        if(element.score_jugador1 > 0 || element.score_jugador2 > 0 ||
            element.id_jugador1 == 0 || element.id_jugador2 == 0){
            //el partido ya se jugo
            //o aun no puede jugarse
            btng.disabled = true;
            btns.disabled = true;
        }

        tdb.appendChild(btng);
        tdb.appendChild(btns);

        tr.appendChild(tdp);
        tr.appendChild(tdj1);
        tr.appendChild(tdj2);
        tr.appendChild(tds1);
        tr.appendChild(tds2);
        tr.appendChild(tdb);

        tbody.appendChild(tr);
    });
}


//==================================== CREAR TORNEOS ====================================//

function sendTorneo(){
    fetch('../crearTorneo', {
        method: 'POST',
        body: JSON.stringify({"Jid":idgame,"URL":urlgame,"Jugadores":array_user}),
        headers:{
          'Content-Type': 'application/json'
        }
    }).then(res => res.json())
    .catch(error => console.error('Error:', error))
    .then(response => location.reload());
}


//==================================== SIMULAR-GENERAR ====================================//
//falta verificarlo
function generar(btn){
    var data = btn.name.split(",");

    fetch('../generarsimular', {
        method: 'POST',
        body: JSON.stringify({"Pid":btn.id,"Jugadores":[data[1],data[2]],"URL":data[0],"Tipo":0}),
        headers:{
          'Content-Type': 'application/json'
        }
    }).then(res => res)
    .catch(error => console.error('Error:', error))
    .then(response => location.reload());
}

function simular(btn){
    var data = btn.name.split(",");

    fetch('../generarsimular', {
        method: 'POST',
        body: JSON.stringify({"Pid":btn.id,"Jugadores":[data[1],data[2]],"URL":data[0],"Tipo":1}),
        headers:{
          'Content-Type': 'application/json'
        }
    }).then(res => res)
    .catch(error => console.error('Error:', error))
    .then(response => location.reload());
}