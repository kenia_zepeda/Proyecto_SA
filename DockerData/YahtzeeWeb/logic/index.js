
function initIndex(){
    var logeado = sessionStorage.getItem('logueado');
    if(!logeado){
        window.location.href = "../";
    }

    //mostrar el nombre del usuario
    var nombre = sessionStorage.getItem('nombre-usuario');
    document.getElementById("nombre_user").innerText = nombre;



    var jid = sessionStorage.getItem('id-usuario');

    fetch('../partidaPendiente', {
        method: 'POST',
        body: JSON.stringify({"Jid":jid}),
        headers:{
          'Content-Type': 'application/json'
        }
    }).then(res => res.json())
    .catch(error => console.error('Error:', error))
    .then(response => pendientes(response));
}


function pendientes(response){
    var array = JSON.parse(JSON.stringify(response));
    //[{partida,jugador1,jugador2,score1,score2}]

    var tbody = document.getElementById("tbody");

    array.forEach(element => {
        var tr = document.createElement("tr");

        var td1 = document.createElement("td");
        td1.innerHTML = element.jugador1;

        var td2 = document.createElement("td");
        td2.innerHTML = element.jugador2;

        var td3 = document.createElement("td");
        td3.innerHTML = element.score1;

        var td4 = document.createElement("td");
        td4.innerHTML = element.score2;
        
        var td5 = document.createElement("td");
        var btn = document.createElement("button");
        btn.setAttribute('id',element.partida);
        btn.setAttribute('onclick','elegirPartida(this);');
        btn.setAttribute("class","d-inline-block btn btn-primary");
        btn.innerText = "Jugar Partida";
        td5.appendChild(btn);

        tr.appendChild(td1);
        tr.appendChild(td2);
        tr.appendChild(td3);
        tr.appendChild(td4);
        tr.appendChild(td5);

        tbody.appendChild(tr);
    });
}


//===================================== INICIAR PARTIDA =====================================//

function elegirPartida(btn){
    //obtener el id de la partida
    var partida = btn.id.toString();
    sessionStorage.setItem('id-partida', partida);

    var id  = sessionStorage.getItem('id-partida');
    var jid = sessionStorage.getItem('id-usuario');

    //agregamos al jugador a la partida
    fetch('../agregarJugador', {
        method: 'POST',
        body: JSON.stringify({"Pid":id,"Jid":jid}),
        headers:{
          'Content-Type': 'application/json'
        }
    }).then(res => res.json())
    .catch(error => console.error('Error:', error))
    .then(response => agregadoPartida(response));
}

function agregadoPartida(response){
    var agregado = response.agregado;

    if(agregado == true){
        window.location.href = "../tablero";
    }
}

function logout(){
    //limpiamos todas las variables de session
    sessionStorage.clear();
    //redirigimos al login
    window.location.href = "../";
}