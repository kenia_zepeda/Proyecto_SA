# SA-Proyecto
Proyecto de Laboratorio - Grupo 5

## Creacion de Bases de Datos
Para saber cuando una base de datos esta lista para ser usada y que no afecte en funcionamiento de los demas contenedores debemos ingresar a sus logs para saber cuando ya termino de crear el conjunto de usuarios y tablas. Para esto vamos a utilizar:

```
>>> sudo docker logs -f containerdb_name
```

Para identificar que la base de datos ya se creo debe salir el listado de archivos .sql que fueron creados y para finalizar el siguiente mensaje:

```
>>> port: 3306  MySQL Community Server - GPL.
```

## Publicar Imagen Docker
Para publicar nuestras imagenes docker debemos loguearnos con nuestro usuario al estilo CLI una vez logueado podremos realizar una copia de nuestra imagen con el nombre con el que se publicara en docker hub y finalmente podremos hacer el push para subir la imagen.

```
>>> sudo docker login
>>> sudo docker tag image:tag user/image:tag
>>> sudo docker push user/image:tag
```

## Archivo .env
Este archivo servira para declarar variables que podemos utilizar dentro de la estructura .yml de docker-compose con el fin de tener un archivo donde se puedan cambiar los parametros para todos los contenedores a crear y con la finalidad de que no tengamos que modificar nuestro archivos .yml. La estructura del archivo .env se puede ver en la copia publicada como .env.dist.

## Diagrama de la interaccion de los contenedores
El servicio de torneo y de usuario se dividio en frontend y backend.

![alt text](./img/diagrama.jpg)


## Comandos importantes
Para el manejo de los contenedores podemos utilizar diferentes comandos con los que podemos ver los logs sel contenedor ademas de que los volumenes tambien crean un historial de las acciones que se realizaron dentro de cada contenedor, entre estos tenemos:

```
>>> sudo docker logs -f container_name
>>> sudo docker exec -it container_name bash
>>> sudo docker exec -it container_name sh
>>> sudo docker build -t name:tag .
```

Para iniciar el torneo debemos utilizar los comandos de docker-compose, ya que todas las imagenes que se utilizan estan en docker hub no es necesario realizar el build de imagenes antes, unicamente se requieren los siguientes comando:

```
>>> sudo docker-compose up -d
>>> sudo docker-compose down
```

Dentro de las bases de datos podemos utilizar los siguientes comandos para acceder a nuestras tablas, en el caso de mysql podemos utiliza:

```
>>> mysql -h localhost -P 3306 --protocol=tcp -u root -p
>>> mysql -u root -p
```

En el caso de mongodb se puede utilizar:
```
>>> mongo
```

Para la limpiesa de la red y volumenes podemos utilizar los siguientes comandos, pero esto borraria tambien los volumnes de las vases de datos.

```
>>> sudo docker container prune
>>> sudo docker volume prune
>>> sudo docker network prune
```